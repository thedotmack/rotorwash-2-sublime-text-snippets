Rotorwash 2 Sublime Text Snippets
=================================



Done
----

rw2_social_twitter_btn( $url, $config=array() )

rw2_social_facebook_btn( $url, $config=array() )

rw2_social_gplus_btn( $url, $config=array() )

rw2_social_pinterest_btn( $url, $img, $config=array() )



To Add
------

rw_add_fb_og_tags()

rw_enqueue_assets()

rw_add_ie_scripts()

rw_add_google_analytics()

rw_add_google_fonts()

rw_filter_wp_title( $title, $separator="&rsaquo;" )

rw_trim_excerpt( $text='' )

rw_excerpt_length( $length=100 )

rw_continue_reading_link()

rw_custom_excerpt_more( $output )

rw_add_custom_post_types( $custom_post_types )

rw_get_wrapper_class()

rw_posted_on()

rw_posted_in( $show_tags=TRUE )

rw_footer_credit_link( $text=NULL, $title=NULL )

rw_create_ga_link($uri, $text = NULL, $title = NULL, $source = 'site-link', $medium = 'generated-link', $campaign = 'rotorwash2', $uri_only = FALSE )

rw_create_short_link( $url )

rw_curl_get_result( $endpoint )

rw2_comment_walker( $comment, $args, $depth )

rw2_comments_form( $args=array(), $post_id=NULL )

rw_admin_init()

rw_has_products()

rw_has_services()

rw_has_testimonials()

rw_radio( $func )

rw_settings_page()

rw_update_dashboard_widgets()

rw_add_dashboard_widget()

rw_progress_alert()

rw_widgets_init()